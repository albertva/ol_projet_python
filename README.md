Projet (code) pour le cours d'outils logiciel

Le projet est un jeu de labyrinthe codé en utilisant la librairie "pygame".
Cette libraire est composé de plusieurs librairies dont celle qui me permet de crée l'interface graphique (display) et celle qui me permet de
détecté l'appuie sur les touches (event) (Z, Q, S, D) pour déplacer le personnage.

Il faut utilisé python 32bits pour que la librairie fonctionne.

Lien vers le site de pygame:
https://www.pygame.org/news

Le code est écrit en utilisant l'IDLE de python 2.7 (32bits).

Afin de mieux comprendre le fonctionnement de la librairie "pygame", j'ai suivi le cours du site openclassroom sur pygame qui propose aussi 
un tp qui consiste à crée un labyrinthe. Je n'ai regardé la correction qu'une fois mon code écrit et testé afin de voir la méthode que les 
dévellopeurs du site utilise pour codé.

lien vers la page "pygame" du site openclassroom:
https://openclassrooms.com/fr/courses/1399541-interface-graphique-pygame-pour-python/1399674-presentation-de-pygame